package AaronLeePersonalProjects.CalorieCount.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class BearerToken {
    @Id
    private String token;

    public String getToken() { return token; }
    public void setToken(String token) {
        this.token = token;
    }
}