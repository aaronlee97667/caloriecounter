package AaronLeePersonalProjects.CalorieCount.food;

import AaronLeePersonalProjects.CalorieCount.entity.UserInfo;
import AaronLeePersonalProjects.CalorieCount.repository.BearerTokenRepository;
import AaronLeePersonalProjects.CalorieCount.repository.UserInfoRepository;
import AaronLeePersonalProjects.CalorieCount.service.JwtService;
import AaronLeePersonalProjects.CalorieCount.service.TokenService;
import jakarta.servlet.http.HttpServletRequest;
import org.antlr.v4.runtime.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;


import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api")
public class FoodController {
    private final FoodService foodService;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    public FoodController(FoodService foodService) {
        this.foodService = foodService;
    }

    @GetMapping
    public List<Food> getFoods() {
        return foodService.getFoods();
    }

    @PostMapping(path = "/food")
    public ResponseEntity<String> addFood(@RequestBody Food food) {
        foodService.addNewFood(food);
        return ResponseEntity.ok("Successfully Added to Database");
    }

    @DeleteMapping(path = "{foodId}")
    public void deleteStudent(@PathVariable("foodId") Long foodId) { foodService.deleteFood(foodId); }

    @PutMapping(path = "{foodId}")
    public void updateStudent(
        @PathVariable("foodId") Long foodId,
        @RequestBody Food updatedFood) {
        foodService.updateFood(foodId, updatedFood);
    }

    @GetMapping("/token")
    public String getToken() {
        return tokenService.getLastToken();
    }

    @PostMapping("/new")
    public String addNewUser(@RequestBody UserInfo userInfo) {
        return foodService.addUser(userInfo);
    }

    @PostMapping("/authenticate")
    public String authenticateAndGetToken(@RequestBody AuthRequest authRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(),authRequest.getPassword()));
        if(authentication.isAuthenticated()) {
            String response = jwtService.generateToken(authRequest.getUsername());
            tokenService.saveToken(response);
            return response;
        } else {
            throw new UsernameNotFoundException("invalid user request!");
        }
    }
}
