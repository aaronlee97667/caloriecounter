package AaronLeePersonalProjects.CalorieCount.food;

import AaronLeePersonalProjects.CalorieCount.entity.UserInfo;
import AaronLeePersonalProjects.CalorieCount.repository.UserInfoRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FoodService {

    @Autowired
    private UserInfoRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final FoodRepository foodRepository;

    @Autowired
    public FoodService(FoodRepository foodRepository) { this.foodRepository = foodRepository; }
    public List<Food> getFoods() { return foodRepository.findAll(); }

    public void addNewFood(Food food) {
        Optional<Food> foodOptional = foodRepository.findFoodByName(food.getName());
        if (foodOptional.isPresent()) {
            throw new IllegalStateException("Food already registered");
        }
        foodRepository.save(food);
    }

    public void deleteFood(Long foodId) {
        boolean exists = foodRepository.existsById(foodId);
        if (!exists) {
            throw new IllegalStateException(
                    "Food with id " + foodId + " does not exist"
            );
        }
        foodRepository.deleteById(foodId);
    }

    @Transactional
    public void updateFood(Long foodId, Food updatedFood) {
        Food food = foodRepository.findById(foodId).orElseThrow(
                () -> new IllegalStateException(
                        "student with id " + foodId + " does not exist"
                )
        );
        food.setName(updatedFood.getName());
        food.setCalories(updatedFood.getCalories());
        food.setProtein(updatedFood.getProtein());
        food.setCarbs(updatedFood.getCarbs());
        food.setFats(updatedFood.getFats());

        foodRepository.save(food);
    }

    public String addUser(UserInfo userInfo) {
        userInfo.setPassword(passwordEncoder.encode(userInfo.getPassword()));
        repository.save(userInfo);
        return "Successfully added user to database";
    }
}