package AaronLeePersonalProjects.CalorieCount.food;

import jakarta.persistence.*;

@Entity
@Table
public class Food {
    @Id
    @SequenceGenerator(
            name = "food_sequence",
            sequenceName = "food_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "food_sequence"
    )
    private Long id;
    private String name;
    private Integer calories;
    private Integer protein;
    private Integer carbs;
    private Integer fats;

    public Food() {

    }
    public Food(
            String name,
            Integer calories,
            Integer protein,
            Integer carbs,
            Integer fats) {
        this.name = name;
        this.calories = calories;
        this.protein = protein;
        this.carbs = carbs;
        this.fats = fats;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getCalories() { return calories; }
    public void setCalories(Integer calories) { this.calories = calories; }

    public Integer getProtein() { return protein; }
    public void setProtein(Integer protein) { this.protein = protein; }
    public Integer getCarbs() { return carbs; }
    public void setCarbs(Integer carbs) { this.carbs = carbs; }
    public Integer getFats() { return fats; }
    public void setFats(Integer fats) { this.fats = fats; }

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", calories='" + calories +
                ", protein='" + protein +
                ", carbs='" + carbs +
                ", fats='" + fats +
                '}';
    }
}
