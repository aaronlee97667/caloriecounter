package AaronLeePersonalProjects.CalorieCount.service;

import AaronLeePersonalProjects.CalorieCount.entity.BearerToken;
import AaronLeePersonalProjects.CalorieCount.repository.BearerTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenService {
    private final BearerTokenRepository tokenRepository;

    @Autowired
    public TokenService(BearerTokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public void saveToken(String token) {
        BearerToken bearerToken = new BearerToken();
        bearerToken.setToken(token);
        tokenRepository.save(bearerToken);
    }

    public String getLastToken() {
        Iterable<BearerToken> tokens = tokenRepository.findAll();
        BearerToken lastToken = null;
        for (BearerToken token : tokens) {
            lastToken = token;
        }
        return lastToken != null ? lastToken.getToken() : null;
    }
}





