package AaronLeePersonalProjects.CalorieCount.repository;

import AaronLeePersonalProjects.CalorieCount.entity.BearerToken;
import org.springframework.data.repository.CrudRepository;

public interface BearerTokenRepository extends CrudRepository<BearerToken, String> {
}
