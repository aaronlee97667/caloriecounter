import { createApp } from 'vue'
import './assets/style.css'
import {createRouter, createWebHistory} from "vue-router";
import App from './App.vue'
import HomeComponent from "./components/HomeComponent.vue";
import LoginComponent from "./components/LoginComponent.vue";
import AddingMealComponent from "./components/AddingMealComponent.vue";
import SignupComponent from "./components/SignupComponent.vue";
import ChartAMealComponent from "./components/ChartAMealComponent.vue";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', name: 'Home', component: HomeComponent},
        {path: '/login', name: 'Login', component: LoginComponent},
        {path: '/add-to-database', name: 'AddtoDatabase', component: AddingMealComponent},
        {path: '/signup', name: 'Signup', component: SignupComponent},
        {path: '/chart', name: 'ChartMeal', component: ChartAMealComponent},
    ]
})

createApp(App).use(router).mount('#app')
