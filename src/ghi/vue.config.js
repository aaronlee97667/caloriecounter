const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = {
    devServer: {
        before: (app) => {
            app.use(
                '/api',
                createProxyMiddleware({
                    target: 'http://localhost:8080', // Replace with your API server URL
                    changeOrigin: true,
                    pathRewrite: {
                        '^/api': '',
                    },
                })
            );
        },
    },
};